# Ansible Challenge - Wednesday Morning

## Part 1

Create a playbook that will apt install figlet, sl, and bastet on bender, fry, and zoidberg.

## Part 2

Update your playbook to use a variable for the packages you want to apt install. Define the values for this variable as play level vars.

## Part 3

Create a vars_file with the values for your variable. Include using the vars_file in your playbook, and remove (or comment out) the play level vars.


